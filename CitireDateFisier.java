
import java.io.BufferedReader;


import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;



public class CitireDateFisier {


	
public  ArrayList<MonitoredData> creatingAListFromFile(){
		 
		 ArrayList<MonitoredData> listFromFile = new ArrayList<>();
     
      try{
		  	BufferedReader buffReader = Files.newBufferedReader(Paths.get("Activities.txt"), StandardCharsets.UTF_8);
		
				Stream<String> linesStream = buffReader.lines();
        
        List activitiesList = linesStream
                          .map(line -> line.split("\\s+") )
                          .map(strings -> new MonitoredData(strings[0]+" "+strings[1],strings[2]+" "+strings[3], strings[4]))
                          .collect(Collectors.toList());
        listFromFile = new ArrayList<>(activitiesList);     
        
      } catch (FileNotFoundException e) {
          System.out.println("File not found.");
          e.printStackTrace();
      } catch (IOException e){
          System.out.println("IO exception.");
          e.printStackTrace();
      }
        
		 
		return listFromFile;
		 
	 }
	 }
	

