

import java.io.FileWriter;
import java.io.IOException;

import java.sql.Time;
import java.util.ArrayList;
import java.util.HashMap;

import java.util.stream.Collectors;




public class MainClass {
	
	public static void main(String[] args) throws IOException  {
		// TODO Auto-generated method stub
	    
		CitireDateFisier citire=new CitireDateFisier();
		
		ArrayList<MonitoredData> newData = new ArrayList<>();
	
				newData = citire.creatingAListFromFile();
				
				 String data = newData.stream().map(mData -> mData.toString()).collect(Collectors.joining("\n-\n"));
				    
				    writeToFile(1, data);

				    
				    
				    
		Func func=new Func();
		
		long distDays=func.distinctDays(newData);
		String s=Long.toString(distDays);
		 writeToFile(2, s);
		 
		 
		 
		HashMap<String, Integer> hashMap = func.appearanceOfEachActivity(newData);
			
        String data3 = hashMap.entrySet().stream().map(mData -> mData.toString()).collect(Collectors.joining("\n\n"));;
	    
	    writeToFile(3, data3);

	    
	    
	    
		HashMap<Integer, HashMap<String,Integer>> hashMap1 = func.countActivitiesPDay(newData);
		
	   
	        String data4 = hashMap1.entrySet().stream().map(mData -> mData.toString()).collect(Collectors.joining("\n\n"));;
		    
		    writeToFile(4, data4);
	    
	    
	    
		HashMap<String, Time> hMap = func.timeForEachActivity(newData);
		
        String data5 = hMap.entrySet().stream().map(mData -> mData.toString()).collect(Collectors.joining("\n\n"));
	    
	    writeToFile(5, data5);
		
		
	}
	
	
	private static void writeToFile(int taskNumb, String data){
        try{
            FileWriter outFile = new FileWriter("Task_"+taskNumb+".txt");
            outFile.write(data);
            outFile.close();

        }catch(IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }

    }

}
