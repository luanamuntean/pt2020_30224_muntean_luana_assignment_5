import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class MonitoredData {

	private Date startTime;
	private Date endTime;
	private String activity;
	
	
	public MonitoredData(Date startTime, Date endTime, String activity) {
		super();
		this.startTime = startTime;
		this.endTime = endTime;
		this.activity = activity;
	}
	
	
	public MonitoredData(String startT, String endT, String activity){
        this.startTime = stringToDate(startT);
        this.endTime = stringToDate(endT);
        this.activity = activity;
    }
	

	
	public Date getStartTime() {
		return startTime;
	}
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
	public Date getEndTime() {
		return endTime;
	}
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	public String getActivity() {
		return activity;
	}
	public void setActivity(String activity) {
		this.activity = activity;
	}
	
	
	@Override
	public String toString() {
		return "Start: "+startTime + "	" + "End: "+endTime +"	"+"Activity: "+activity;
	}
	
	
	
	
	
	public Date stringToDate(String stringDate){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yy-MM-dd HH:mm:ss");
        Date date = null;
        try {
            date = dateFormat.parse(stringDate);
        }catch(ParseException e){
            e.printStackTrace();
        }
        return date;

    }
	
	
}
