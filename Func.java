import java.sql.Time;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class Func {
	
//TASK2
	
	public long distinctDays(ArrayList<MonitoredData> activities)
	{
  
		long distDays = activities.stream()
	    		.map(monitData -> monitData.getStartTime().getDate())
	        .distinct()
	        .count();
			
			return distDays;

		
		
		
	}
	
	

	//TASK3
	public HashMap<String,Integer> appearanceOfEachActivity(List<MonitoredData>activities){
		
		HashMap<String, Integer> newHashMap = new HashMap<String, Integer>();
           
		activities.stream().forEach((currentActivity) ->{
			
			if(newHashMap.containsKey(currentActivity.getActivity())) {
				newHashMap.replace(currentActivity.getActivity(), newHashMap.get(currentActivity.getActivity()), newHashMap.get(currentActivity.getActivity()).intValue()+1);
			}
			else
			{
				newHashMap.put(currentActivity.getActivity(), new Integer(1));
			}
			
		});
		
		return newHashMap;
		
	}
	
	
	
	
	
	//TASK4
	 public HashMap<Integer, HashMap<String,Integer>> countActivitiesPDay(ArrayList<MonitoredData> activities){

	        HashMap<Integer, HashMap<String,Integer>> activsPDay = new HashMap<>();

	        //cream o lista cu zilele distincte care apar in fisierul Activities.txt
	        List<Integer> distDays =
	                activities.stream()
	                        .map(monitData -> monitData.getStartTime().getDate())
	                        .distinct()
	                        .collect(Collectors.toList());

	        HashMap<Integer, List<MonitoredData>> activsListPDay = new HashMap<>();

	        distDays.stream()
	                .forEach(distD -> activsListPDay.put(distD,	
	                                    activities.stream()
	                                    .filter(monitD -> monitD.getStartTime().getDate()== distD )
	                                    .collect(Collectors.toList())
	                ));
	                
	        activsListPDay.entrySet().stream()
	        		.forEach(entry -> activsPDay.put(entry.getKey(), appearanceOfEachActivity(entry.getValue()) ) );



	        return activsPDay;
	    }
	
		
 
	
	//TASK5
	
	 public HashMap<String, Time>timeForEachActivity(ArrayList<MonitoredData> activities)
	  { 
		  HashMap<String,Time> hashMap = new HashMap<String, Time>();
	  activities.stream().forEach(monData -> {
		  long recordTimeInMillis =monData.getEndTime().getTime() - monData.getStartTime().getTime(); 
		  Time timeForThisRecord = new Time(recordTimeInMillis -TimeUnit.HOURS.toMillis(2));
		  
		  System.out.println(monData.toString() +"\t\tDuration of activity: "+timeForThisRecord.toString());
	    
	      hashMap.put(monData.toString(),timeForThisRecord); 
	  
	      });
	  return hashMap;
	  }
	 
	
		
	}

	

